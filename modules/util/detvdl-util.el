;;; detvdl-util.el --- Util module specification
;;; Commentary:
;;; Code:
(require 'detvdl-common)
(require 'detvdl-smartparens)
(require 'detvdl-ivy)
(require 'detvdl-projectile)
(require 'detvdl-git)
(require 'detvdl-history)
(require 'detvdl-erc)
(require 'detvdl-rest)

(provide 'detvdl-util)
;;; detvdl-util.el ends here
