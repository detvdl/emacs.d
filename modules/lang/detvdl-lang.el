;;; detvdl-lang.el --- Language modules specification
;;; Commentary:
;;; Code:

(require 'detvdl-programming)
(require 'detvdl-company)
(require 'detvdl-yasnippet)

(require 'detvdl-shell-mode)
(require 'detvdl-ruby)
(require 'detvdl-common-lisp)
(require 'detvdl-elisp)
;; (require 'detvdl-elixir)
;; (require 'detvdl-haskell)
(require 'detvdl-rust)
(require 'detvdl-java)
(require 'detvdl-clojure)
(require 'detvdl-python)
(require 'detvdl-web)
(require 'detvdl-js)
(require 'detvdl-c)
;; (require 'detvdl-c++)

(require 'detvdl-debugger)
(require 'detvdl-ess)

(require 'detvdl-org)
(require 'detvdl-latex)
(require 'detvdl-markup)

(provide 'detvdl-lang)
;;; detvdl-lang.el ends here
