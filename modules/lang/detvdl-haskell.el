;;; detvdl-haskell.el --- Haskell packages and configuration
;;; Commentary:
;;; Code:

(use-package haskell-mode
  :ensure t
  :commands haskell-mode)

(provide 'detvdl-haskell)
;;; detvdl-haskell.el ends here
